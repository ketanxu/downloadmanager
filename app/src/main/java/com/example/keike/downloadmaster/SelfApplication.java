package com.example.keike.downloadmaster;

import android.app.Application;
import android.content.Context;

/**
 * Created by ketan on 2017/6/5.
 */

public class SelfApplication extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = SelfApplication.this;
    }


    public static Context getContext() {
        return mContext;
    }

}
