package com.example.keike.downloadmaster.download;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;

/**
 * Created by ketan on 2017/6/5.
 */

public class DownLoadProgress extends ProgressDialog {
    private DownloadTask mTask;
    private String mUrl;
    private Handler progress = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == DownloadTask.PROCESS) {
                setProgress((Integer) msg.obj);
            }
            return true;
        }
    });

    public DownLoadProgress(Context context) {
        this(context, 0);
    }

    public DownLoadProgress(Context context, int theme) {
        super(context, theme);
        initView();
    }

    private void initView() {
        setTitle("正在下载升级包。。。");
        setMax(100);
        setCancelable(false);
        setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        setButton(BUTTON_NEGATIVE, "浏览器下载", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DownloadUtil.create(getContext()).downloadByWeb(mUrl);

                //
                mTask.destroyTask();

                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
    }

    public void showProgress(String url, String loadTitle) {
        this.mUrl = url;
        mTask = new DownloadTask(url, loadTitle, progress, true);
        mTask.start();
        show();
    }


}
