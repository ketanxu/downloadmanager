package com.example.keike.downloadmaster;
import android.Manifest;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.example.keike.downloadmaster.download.DownLoadProgress;

public class MainActivity extends AppCompatActivity {
    final String DOWNLOAD_URL = "https://ucan.25pp.com/Wandoujia_web_seo_baidu_homepage.apk";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.bt_download).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean checkPermission = ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == 0 ? true : false;
                if (checkPermission) {
                    DownLoadProgress progress = new DownLoadProgress(MainActivity.this);
                    progress.showProgress(DOWNLOAD_URL, "demo.apk");
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 12001);
                }
            }

        });
    }


}
